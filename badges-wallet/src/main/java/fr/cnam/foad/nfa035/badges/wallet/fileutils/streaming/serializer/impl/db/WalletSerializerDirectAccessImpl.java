package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;


/**
 * Serialiseur de badge en accès direct
 */
public class WalletSerializerDirectAccessImpl extends AbstractStreamingImageSerializer <File, WalletFrameMedia> {

    /**
     * Sérialise une image depuis un support quelconque vers un media quelconque
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(File source, WalletFrameMedia media) throws IOException {
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            long numberOfLines = media.getNumberOfLines();
            PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
            writer.printf("%1$d;", size);
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                eos.flush();
            }
            writer.printf("\n");
            writer.printf("%1$d;%2$d;",numberOfLines + 2, media.getChannel().getFilePointer());
        }
        media.incrementLines();
    }

    /**
     * Utile pour récupérer un Flux de lecture de la source à sérialiser
     *
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws IOException {
        return new FileInputStream(source) ;
    }

    /**
     * Permet de récupérer le flux d'écriture et de sérialisation vers le media
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public <T extends OutputStream> T getSerializingStream(WalletFrameMedia media) throws IOException {
        return null;
    }
}
