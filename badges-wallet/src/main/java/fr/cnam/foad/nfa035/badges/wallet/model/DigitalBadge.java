package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;

public class DigitalBadge {
    // Déclaration des variables de classe
    File badge ;
    DigitalBadgeMetadata metadata;

    /**
     * Constructeur de classe
     */
    public DigitalBadge(File badge, DigitalBadgeMetadata metadata) {
        this.badge=badge;
        this.metadata = metadata;
    }

    /**
     * Todo
     * @return
     */
    public boolean equals() {
        return false ;
    }

    // Getter du badge
    public File getBadge () {
        return badge;
    }

    // Setter du badge
    public void setBadge (File newBadge) {
        this.badge=newBadge;
    }

    // Getter des metadonnées
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    // Setter de metadonnées
    public void setMetadata(DigitalBadgeMetadata newMetada) {
        this.metadata=newMetada;
    }

    public String toString () {

        return null;
    }



}
