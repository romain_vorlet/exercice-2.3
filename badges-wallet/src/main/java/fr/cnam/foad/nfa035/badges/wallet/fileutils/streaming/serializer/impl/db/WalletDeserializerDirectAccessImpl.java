package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);
    List metas = new List<DigitalBadgeMetadata>;
    OutputStream sourceOutputStream = null ;



    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {

    }

    /**
     * A partir d'un set de métadonnées, désérialiser le badge concernée.
     * @param media
     * @param meta
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }

    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return null;
    }


    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return null;
    }

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

    }
}
