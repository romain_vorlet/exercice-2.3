package fr.cnam.foad.nfa035.badges.wallet.model;

public class DigitalBadgeMetadata {

    // Déclaration des variables de classe
    int badgeId ;
    long imageSize ;
    long walletPosition;

    // Constructeur de classe
    public DigitalBadgeMetadata (int badgeId, long imageSize, long walletPosition) {
        this.badgeId = badgeId ;
        this.imageSize=imageSize;
        this.walletPosition=walletPosition;
    }

    public boolean equals () {
        return false ;
    }

    // Getter de l'ID du badge
    public int getBadgeId() {
        return badgeId;
    }

    // Setter de l'ID du badge
    public void setBadgeId (int newBadgeId) {
        this.badgeId=newBadgeId;
    }

    // Getter de la taille de l'image (en octet)
    public long getImageSize() {
        return imageSize;
    }

    // Setter de la taille de l'image (en octet)
    public void setImageSize (long newImageSize) {
        this.imageSize=newImageSize;
    }

    // Getter de la position du badge dans la BDD
    public long getWalletPosition() {
        return walletPosition;
    }

    // Setter de la position du badge dans la BDD
    public void setWalletPosition (long newWalletPosition) {
        this.walletPosition=newWalletPosition;
    }

    public String toString () {
        return null;
    }

}
