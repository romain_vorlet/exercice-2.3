package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {

        // A COMPLéTER
    }

    /**
     * Parcours du flux et récupération de la dernière ligne :
     * 1. On charge le taille du fichier
     * 2. Positionnement du pointeur de lecture au dernier byte
     * 3. Ecriture des données en sens inverse et jusqu'au début du fichier ou la ligne précédente
     * 4. Comme on lit en sens inverse, il faut alors inverser la chaîne de caractère obtenue.
     *
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
