package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;


public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);
    private final File walletDatabase;
    //private BadgeDeserializer deserializer = new ImageStreamingDeserializer<ResumableImageFrameMedia>();

    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * afin de mettre à jour l'amorce à chaque ajout de badge au Wallet
     */
    @Override
    public void addBadge(File image) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl();
            serializer.serialize(image, media);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        public void getBadge (OutputStream imageStream) throws IOException {
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
                new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
            }

            catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Pour récupérer l'ensemble des métadonnées du Wallet par balayage en flux de toutes les lignes du fichier csv
         * Todo : lecture séquentielle à prévoir
         */
        @Override
        public List<DigitalBadgeMetadata> getWalletMetadata () throws IOException {
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
                /**
                 * Lire les métadonnées de chaque badge et les insérer dans une liste
                 *  1. Lire les lignes de la BDD en accès séquentiel,
                 *  2. Récupérer l'information en position 1-2 de la variable où l'on stocke ces informations,
                 *  3. Créer un objet de type DigitalBadgeMetada qui pourra être stocké dans la liste
                 *  Le problème, c'est que je ne lis qu'une seule ligne et que je gère mal la variable walletPosition de
                 *  l'objet DigitalBadgeMedata. En l'état, cela ne peut par marcher, il me faut une boucle type While,
                 *  probablement basé sur le pointeur pour l'arrêt de la boucle
                 *  */

                BufferedReader br = media.getEncodedImageReader(true);
                String[] data = br.readLine().split(";");
                List<DigitalBadgeMetadata> metaList = null;
                DigitalBadgeMetadata Metadonnees = new DigitalBadgeMetadata(Integer.parseInt(data[0]), Long.parseLong(data[1]), Long.parseLong(data[2]));
                metaList.add(Metadonnees);



            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * pour récupérer un seul et unique Badge par accès direct.
         */
        @Override
        public void getBadgeFromMetadata (OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
            List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
                /**
                 * A partir d'une métadonnée, récupérer le badge correspondant
                 * 1. Désérialiser les métadonnées
                 * 2. Se rendre à la position ou trouver l'ID correspondant
                 * 3. Récupérer le badge et le désérialiser
                 * 4. La méthode étant void, le badge est affiché ou un fichier directement créé
                 *
                 */

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

